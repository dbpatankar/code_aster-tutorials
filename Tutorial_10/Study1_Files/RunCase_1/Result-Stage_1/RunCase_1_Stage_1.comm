DEBUT()


beam = LIRE_MAILLAGE(UNITE=20)

cutsec = LIRE_MAILLAGE(UNITE=2)

model = AFFE_MODELE(
    AFFE=_F(
        GROUP_MA=('BEAM', ), MODELISATION=('POU_D_EM', ), PHENOMENE='MECANIQUE'
    ),
    MAILLAGE=beam
)

fiber = DEFI_GEOM_FIBRE(
    FIBRE=_F(
        ANGLE=-90.0,
        CARA='DIAMETRE',
        COOR_AXE_POUTRE=(0.0, 0.0),
        GROUP_FIBRE='rf',
        VALE=(
            0.1, 0.2, 0.032, -0.1, 0.2, 0.032, 0.1, -0.2, 0.01, -0.1, -0.2, 0.01
        )
    ),
    SECTION=_F(
        ANGLE=-90.0,
        COOR_AXE_POUTRE=(0.0, 0.0),
        GROUP_FIBRE='concf',
        MAILLAGE_SECT=cutsec,
        TOUT_SECT='OUI'
    )
)

elemprop = AFFE_CARA_ELEM(
    GEOM_FIBRE=fiber,
    MODELE=model,
    MULTIFIBRE=_F(GROUP_FIBRE=('concf', 'rf'), GROUP_MA=('BEAM', )),
    ORIENTATION=_F(CARA='VECT_Y', GROUP_MA=('BEAM', ), VALE=(0.0, 0.0, 1.0)),
    POUTRE=_F(
        CARA=('HY', 'HZ'),
        GROUP_MA=('BEAM', ),
        SECTION='RECTANGLE',
        VALE=(0.6, 0.3)
    )
)

steel = DEFI_MATERIAU(ELAS=_F(E=2.1e+11, NU=0.3))

conc = DEFI_MATERIAU(ELAS=_F(E=30000000000.0, NU=0.2))

listr = DEFI_LIST_REEL(DEBUT=0.0, INTERVALLE=_F(JUSQU_A=1.0, PAS=1.0))

bc = AFFE_CHAR_MECA(
    DDL_IMPO=_F(GROUP_NO=('FIX', ), LIAISON='ENCASTRE'), MODELE=model
)

load0 = AFFE_CHAR_MECA(
    FORCE_POUTRE=_F(FZ=-10000.0, GROUP_MA=('BEAM', ), TYPE_CHARGE='FORCE'),
    MODELE=model
)

behav = DEFI_COMPOR(
    GEOM_FIBRE=fiber,
    MATER_SECT=conc,
    MULTIFIBRE=(
        _F(GROUP_FIBRE=('concf', ), MATER=conc, RELATION='ELAS'),
        _F(GROUP_FIBRE=('rf', ), MATER=steel, RELATION='ELAS')
    )
)

fieldmat = AFFE_MATERIAU(
    AFFE=_F(GROUP_MA=('BEAM', ), MATER=(conc, steel)),
    AFFE_COMPOR=_F(COMPOR=behav, GROUP_MA=('BEAM', )),
    MODELE=model
)

resnonl = STAT_NON_LINE(
    CARA_ELEM=elemprop,
    CHAM_MATER=fieldmat,
    COMPORTEMENT=_F(RELATION='MULTIFIBRE'),
    EXCIT=(_F(CHARGE=bc), _F(CHARGE=load0)),
    INCREMENT=_F(LIST_INST=listr),
    MODELE=model
)

IMPR_RESU(FORMAT='MED', RESU=_F(RESULTAT=resnonl), UNITE=80)

IMPR_RESU_SP(
    GROUP_MA=('BEAM', ),
    RESU=_F(NOM_CHAM='SIEF_ELGA', NOM_CMP=('SIXX', )),
    RESULTAT=resnonl,
    UNITE=3
)

FIN()