DEBUT()

# Young's modulus
YM = 2.1e+11

# Poisson's ratio
MU = 0.2

# Load
P = -100000

mesh = LIRE_MAILLAGE(FORMAT='MED', UNITE=20)

model = AFFE_MODELE(
    AFFE=_F(
        GROUP_MA=('BEAM', ), MODELISATION=('POU_D_E', ), PHENOMENE='MECANIQUE'
    ),
    MAILLAGE=mesh
)

elemprop = AFFE_CARA_ELEM(
    MODELE=model,
    ORIENTATION=_F(CARA='VECT_Y', GROUP_MA=('BEAM', ), VALE=(0.0, 0.0, 1.0)),
    POUTRE=_F(
        CARA=('HY', 'HZ', 'EPY', 'EPZ'),
        GROUP_MA=('BEAM', ),
        SECTION='RECTANGLE',
        VALE=(0.4, 0.2, 0.2, 0.1)
    )
)

mater = DEFI_MATERIAU(ELAS=_F(E=YM, NU=MU))

fieldmat = AFFE_MATERIAU(
    AFFE=_F(GROUP_MA=('BEAM', ), MATER=(mater, )), MAILLAGE=mesh, MODELE=model
)

load = AFFE_CHAR_MECA(
    DDL_IMPO=_F(GROUP_NO=('FIX', ), LIAISON='ENCASTRE'),
    FORCE_NODALE=_F(FZ=P, GROUP_NO=('LOAD', )),
    MODELE=model
)

reslin = MECA_STATIQUE(
    CARA_ELEM=elemprop,
    CHAM_MATER=fieldmat,
    EXCIT=_F(CHARGE=load),
    MODELE=model
)

table = POST_RELEVE_T(
    ACTION=_F(
        INTITULE='defl',
        GROUP_NO='DISP',
        NOM_CHAM='DEPL',
        OPERATION=('EXTRACTION', ),
        RESULTANTE=('DZ', ),
        RESULTAT=reslin
    )
)

IMPR_TABLE(FORMAT='TABLEAU', TABLE=table, UNITE=10)

IMPR_RESU(FORMAT='MED', RESU=_F(RESULTAT=reslin), UNITE=80)

FIN()
